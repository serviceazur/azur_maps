var map;
function initialize() {
  
  if(jQuery('div').is('#map-canvas')){
    
    var lat = parseFloat(Drupal.settings.azur_maps_lat);
    var lng = parseFloat(Drupal.settings.azur_maps_lng);
    
    // устанавливаем координаты центра из настроек в панели управления 
    var center = new google.maps.LatLng(lat, lng);
    
    var mapOptions = {
      zoom: 10, //set default zoom level
      center: center,
      mapTypeId: google.maps.MapTypeId.ROADMAP //set default map type(ROADMAP,SATELLITE,HYBRID,TERRAIN)
    };
    
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions); //***ORIGINAL***

    circleOptions = {
      center: {lat: lat, lng: lng},
      strokeColor: '#FF0000',
      strokeOpacity: 0.2,
      strokeWeight: 0,
      fillColor: '#FF0000',
      fillOpacity: 0.2,
      map: map,
      radius: 5000
    };
    
    //рисуем окружность
    circle = new google.maps.Circle(circleOptions);
    circle.setMap(map);
    
  }
  
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' + 'callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;